#!/bin/bash

USAGE="Usage: "`basename $0`" <Lab/Room> <user> <OVA Files...>"

if [ $# -lt 3 ]; then
	echo $USAGE
	exit 1
fi

LAB=$1
USER=$2
# Remove the lab number from $@
shift 2

OVAS=$@


uploadAndImport() {
	TARGET=$1
	# Remove the machine from $@
	shift 1

	for image in $@
	do
		# TARGET may have directories on it, and we want just the filename
		FILENAME=`basename $image`
		scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $image $TARGET:.
		ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $TARGET "VBoxManage import $FILENAME" &
	done
}

for m in `seq 1 24`
do
	mach=`printf "$USER@$LAB-%02d.trainctrs.trng" $m`

	uploadAndImport $mach $OVAS
done

#uploadAndImport $USER@$LAB-inst.trainctrs.trng $OVAS
